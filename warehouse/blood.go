package warehouse

type DivineBlood struct {
	powderReagent PowderReagent
	powderOfSoil  string
	fairyLeaf     string
	bloodGroupOne string
}

func NewDivineBlood() DivineBlood {
	NewDivineBlood := DivineBlood{
		powderReagent: NewPowderReagent(),
		powderOfSoil:  powderOfSoil,
		fairyLeaf:     fairyLeaf,
		bloodGroupOne: bloodGroupOne,
	}
	return NewDivineBlood
}

func (divineBlood *DivineBlood) CountSimpleReagents() map[string]int {
	powderReagentRoster := divineBlood.powderReagent.CountSimpleReagents()
	roster := map[string]int{
		divineBlood.powderOfSoil:  1,
		divineBlood.fairyLeaf:     1,
		divineBlood.bloodGroupOne: 2,
	}
	for key := range powderReagentRoster {
		roster[key] += powderReagentRoster[key] * 1
	}
	return roster
}

type SinnerBlood struct {
	liquidReagent    LiquidReagent
	fireDust         string
	knotOfBloodyTree string
	bloodGroupFour   string
}

func NewSinnerBlood() SinnerBlood {
	NewSinnerBlood := SinnerBlood{
		liquidReagent:    NewLiquidReagent(),
		fireDust:         fireDust,
		knotOfBloodyTree: knotOfBloodyTree,
		bloodGroupFour:   bloodGroupFour,
	}
	return NewSinnerBlood
}

func (sinnerBlood *SinnerBlood) CountSimpleReagents() map[string]int {
	liquidReagentRoster := sinnerBlood.liquidReagent.CountSimpleReagents()
	roster := map[string]int{
		sinnerBlood.fireDust:         1,
		sinnerBlood.knotOfBloodyTree: 1,
		sinnerBlood.bloodGroupFour:   2,
	}
	for key := range liquidReagentRoster {
		roster[key] += liquidReagentRoster[key] * 1
	}
	return roster
}

type SageBlood struct {
	liquidReagent  LiquidReagent
	powderOfRepair string
	monkBranch     string
	bloodGroupFive string
}

func NewSageBlood() SageBlood {
	NewSageBlood := SageBlood{
		liquidReagent:  NewLiquidReagent(),
		powderOfRepair: powderOfRepair,
		monkBranch:     monkBranch,
		bloodGroupFive: bloodGroupFive,
	}
	return NewSageBlood
}

func (sageBlood *SageBlood) CountSimpleReagents() map[string]int {
	liquidReagentRoster := sageBlood.liquidReagent.CountSimpleReagents()
	roster := map[string]int{
		sageBlood.powderOfRepair: 1,
		sageBlood.monkBranch:     1,
		sageBlood.bloodGroupFive: 2,
	}
	for key := range liquidReagentRoster {
		roster[key] += liquidReagentRoster[key] * 1
	}
	return roster
}

type JesterBlood struct {
	liquidReagent   LiquidReagent // 1
	dustOfDarkness  string        // 1
	fairyLeaf       string        // 1
	bloodGroupThree string        // 2
}

func NewJesterBlood() JesterBlood {
	NewJesterBlood := JesterBlood{
		liquidReagent:   NewLiquidReagent(),
		dustOfDarkness:  dustOfDarkness,
		fairyLeaf:       fairyLeaf,
		bloodGroupThree: bloodGroupThree,
	}
	return NewJesterBlood
}

func (jesterBlood *JesterBlood) CountSimpleReagents() map[string]int {
	liquidReagentRoster := jesterBlood.liquidReagent.CountSimpleReagents()
	roster := map[string]int{
		jesterBlood.dustOfDarkness:  1,
		jesterBlood.fairyLeaf:       1,
		jesterBlood.bloodGroupThree: 2,
	}
	for key := range liquidReagentRoster {
		roster[key] += liquidReagentRoster[key] * 1
	}
	return roster
}
