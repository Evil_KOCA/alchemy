package warehouse

import "fmt"

const (
	// SimpleReagents
	wainingMoonTears = "Слезы Убывающей Луны"
	cleanWater       = "Очищенная Вода"
	calendula        = "Календула"
	littleHPPotion   = "Зелье HP(малое)"
	wavelet          = "Волнушка"
	monkBranch       = "Ветвь Монаха"
	powderOfOrigin   = "Порошок Изначальности"
	fireDust         = "Пыль Огня"
	pineJuice        = "Сок Сосны"
	clover           = "Клевер"
	dustOfDarkness   = "Пыль Тьмы"
	fairyLeaf        = "Волшебный Плод"
	natureFruit      = "Плод Природы"
	sugar            = "Сахар"
	salt             = "Соль"
	weed             = "Сорняк"
	azalea           = "Азалея"
	dustOfAges       = "Пыль Веков"
	mapleJuice       = "Сок Клена"
	ashJuice         = "Сок Ясеня"
	alderJuice       = "Сок Ольхи"
	edulis           = "Боровик"
	lotus            = "Лотос"
	powderOfRepair   = "Порошок Починки"
	knotOfBloodyTree = "Сучок Кровавого Дерева"
	bloodGroupOne    = "Кровь I Группы"
	bloodGroupThree  = "Кровь III Группы"
	bloodGroupFour   = "Кровь IV Группы"
	bloodGroupFive   = "Кровь V Группы"
	powderOfSoil     = "Порошок Земли"
)

type GreenPotion struct {
	disciplePotion            DisciplePotion //3
	workerPotion              WorkerPotion   //3
	speedPotion               SpeedPotion    //3
	wainingMoonTears          string         //1
	requireMasteryGrade       int
	strongVersionMasteryGrade int
}

func NewGreenPotion() GreenPotion {
	NewGreenPotion := GreenPotion{
		disciplePotion:            NewDisciplePotion(),
		workerPotion:              NewWorkerPotion(),
		speedPotion:               NewSpeedPotion(),
		wainingMoonTears:          wainingMoonTears,
		requireMasteryGrade:       0,
		strongVersionMasteryGrade: 0,
	}
	return NewGreenPotion
}

func (gp *GreenPotion) CountSimpleReagents() map[string]int {
	disciplePotionRoster := gp.disciplePotion.CountSimpleReagents()
	workerPotionRoster := gp.workerPotion.CountSimpleReagents()
	speedPotionRoster := gp.speedPotion.CountSimpleReagents()
	roster := map[string]int{
		gp.wainingMoonTears: 1,
	}
	for key := range disciplePotionRoster {
		roster[key] += disciplePotionRoster[key] * 3
	}
	for key := range workerPotionRoster {
		roster[key] += workerPotionRoster[key] * 3
	}
	for key := range speedPotionRoster {
		roster[key] += speedPotionRoster[key] * 3
	}
	return roster
}

type TryInterface interface {
	GetRequirements() (int, int)
}

type SpeedPotion struct {
	divineBlood        DivineBlood //1
	dustOfDarkness     string      //2
	alderJuice         string      //5
	edulis             string      //5
	commonMasteryGrade int         //55
	strongMasteryGrade int         //155
	commonExpGain      int         //1200
	strongExpGain      int         //1500
}

func (sp *SpeedPotion) GetRequirements() (requireMasteryGrade, strongVersionMasteryGrade int) {
	return sp.commonMasteryGrade, sp.strongMasteryGrade
}

func NewSpeedPotion() SpeedPotion {
	NewSpeedPotion := SpeedPotion{
		divineBlood:        NewDivineBlood(),
		dustOfDarkness:     dustOfDarkness,
		alderJuice:         alderJuice,
		edulis:             edulis,
		commonMasteryGrade: 55,
		strongMasteryGrade: 155,
		commonExpGain:      1200,
		strongExpGain:      1500,
	}
	return NewSpeedPotion
}

func (sp *SpeedPotion) CountSimpleReagents() map[string]int {
	sinnerBloodRoster := sp.divineBlood.CountSimpleReagents()
	roster := map[string]int{
		sp.dustOfDarkness: 2,
		sp.alderJuice:     5,
		sp.edulis:         5,
	}
	for key := range sinnerBloodRoster {
		roster[key] += sinnerBloodRoster[key] * 1
	}
	return roster
}

type WorkerPotion struct {
	sinnerBlood SinnerBlood //1
	fireDust    string      //2
	ashJuice    string      //4
	azalea      string      //6
}

func NewWorkerPotion() WorkerPotion {
	NewWorkerPotion := WorkerPotion{
		sinnerBlood: NewSinnerBlood(),
		fireDust:    fireDust,
		ashJuice:    ashJuice,
		azalea:      azalea,
	}
	return NewWorkerPotion
}

func (workerPotion *WorkerPotion) CountSimpleReagents() map[string]int {
	sinnerBloodRoster := workerPotion.sinnerBlood.CountSimpleReagents()
	roster := map[string]int{
		workerPotion.fireDust: 2,
		workerPotion.ashJuice: 4,
		workerPotion.azalea:   6,
	}
	for key := range sinnerBloodRoster {
		roster[key] += sinnerBloodRoster[key] * 1
	}
	return roster
}

type DisciplePotion struct {
	sageBlood  SageBlood //1
	dustOfAges string    //1
	mapleJuice string    //2
	lotus      string    //6
}

func NewDisciplePotion() DisciplePotion {
	NewDisciplePotion := DisciplePotion{
		sageBlood:  NewSageBlood(),
		dustOfAges: dustOfAges,
		mapleJuice: mapleJuice,
		lotus:      lotus,
	}
	return NewDisciplePotion
}

func (disciplePotion *DisciplePotion) CountSimpleReagents() map[string]int {
	sageBloodRoster := disciplePotion.sageBlood.CountSimpleReagents()
	roster := map[string]int{
		disciplePotion.dustOfAges: 2,
		disciplePotion.mapleJuice: 5,
		disciplePotion.lotus:      6,
	}
	for key := range sageBloodRoster {
		roster[key] += sageBloodRoster[key] * 1
	}
	return roster
}

type BeastPotion struct {
	messengerPotion  MessengerPotion  // 3
	experiencePotion ExperiencePotion // 3
	huntPotion       HuntPotion       // 3
	wainingMoonTears string           // 1
}

func NewBeastPotion() BeastPotion {
	NewBeastPotion := BeastPotion{
		messengerPotion:  NewMessengerPotion(),
		experiencePotion: NewExperiencePotion(),
		huntPotion:       NewHuntPotion(),
		wainingMoonTears: wainingMoonTears,
	}
	return NewBeastPotion
}

func (beastPotion *BeastPotion) CountSimpleReagents() map[string]int {
	messengerPotionRoster := beastPotion.messengerPotion.CountSimpleReagents()
	experiencePotionRoster := beastPotion.experiencePotion.CountSimpleReagents()
	huntPotionRoster := beastPotion.huntPotion.CountSimpleReagents()
	roster := map[string]int{
		beastPotion.wainingMoonTears: 1,
	}
	for key := range messengerPotionRoster {
		roster[key] += messengerPotionRoster[key] * 3
	}
	for key := range experiencePotionRoster {
		roster[key] += experiencePotionRoster[key] * 3
	}
	for key := range huntPotionRoster {
		roster[key] += huntPotionRoster[key] * 3
	}
	return roster
}

type RegenerationPotion struct {
	liquidReagent             LiquidReagent //1
	cleanWater                string        //3
	calendula                 string        //3
	littleHPPotion            string        //2
	requireMasteryGrade       int           //0
	strongVersionMasteryGrade int           //105
}

func NewRegenerationPotion() RegenerationPotion {
	NewRegenerationPotion := RegenerationPotion{
		liquidReagent:             NewLiquidReagent(),
		cleanWater:                cleanWater,
		calendula:                 calendula,
		littleHPPotion:            littleHPPotion,
		requireMasteryGrade:       0,
		strongVersionMasteryGrade: 105,
	}
	return NewRegenerationPotion
}

func (regenerationPotion *RegenerationPotion) CountSimpleReagents() map[string]int {
	liquidReagentRoster := regenerationPotion.liquidReagent.CountSimpleReagents()
	roster := map[string]int{
		regenerationPotion.cleanWater:     3,
		regenerationPotion.calendula:      3,
		regenerationPotion.littleHPPotion: 3,
	}
	for key := range liquidReagentRoster {
		roster[key] += liquidReagentRoster[key] * 1
	}
	return roster
}

type MessengerPotion struct {
	powderReagent  PowderReagent // 4
	durabilityOil  DurabilityOil // 1
	wavelet        string        // 2
	monkBranch     string        // 2
	powderOfOrigin string        // 4
}

func NewMessengerPotion() MessengerPotion {
	NewMessengerPotion := MessengerPotion{
		powderReagent:  NewPowderReagent(),
		durabilityOil:  NewDurabilityOil(),
		wavelet:        wavelet,
		monkBranch:     monkBranch,
		powderOfOrigin: powderOfOrigin,
	}
	return NewMessengerPotion
}

func (messengerPotion *MessengerPotion) CountSimpleReagents() map[string]int {
	powderReagentRoster := messengerPotion.powderReagent.CountSimpleReagents()
	durabilityOilRoster := messengerPotion.durabilityOil.CountSimpleReagents()
	roster := map[string]int{
		messengerPotion.wavelet:        2,
		messengerPotion.monkBranch:     2,
		messengerPotion.powderOfOrigin: 4,
	}
	for key := range powderReagentRoster {
		roster[key] += powderReagentRoster[key] * 4
	}
	for key := range durabilityOilRoster {
		roster[key] += durabilityOilRoster[key] * 1
	}
	return roster
}

type ExperiencePotion struct {
	jesterBlood JesterBlood // 1
	fireDust    string      // 2
	pineJuice   string      // 5
	clover      string      // 7
}

func NewExperiencePotion() ExperiencePotion {
	NewExperiencePotion := ExperiencePotion{
		jesterBlood: NewJesterBlood(),
		fireDust:    fireDust,
		pineJuice:   pineJuice,
		clover:      clover,
	}
	return NewExperiencePotion
}

func (experiencePotion *ExperiencePotion) CountSimpleReagents() map[string]int {
	jesterBloodRoster := experiencePotion.jesterBlood.CountSimpleReagents()
	roster := map[string]int{
		experiencePotion.fireDust:  2,
		experiencePotion.pineJuice: 5,
		experiencePotion.clover:    7,
	}
	for key := range jesterBloodRoster {
		roster[key] += jesterBloodRoster[key] * 1
	}
	return roster
}

type HuntPotion struct {
	powderReagent   PowderReagent // 1
	cleanWater      string        // 3
	bloodGroupThree string        // 6
	calendula       string        // 4
}

func NewHuntPotion() HuntPotion {
	NewHuntPotion := HuntPotion{
		powderReagent:   NewPowderReagent(),
		cleanWater:      cleanWater,
		bloodGroupThree: bloodGroupThree,
		calendula:       calendula,
	}
	return NewHuntPotion
}

func (huntPotion *HuntPotion) CountSimpleReagents() map[string]int {
	powderReagentRoster := huntPotion.powderReagent.CountSimpleReagents()
	roster := map[string]int{
		huntPotion.cleanWater:      3,
		huntPotion.bloodGroupThree: 6,
		huntPotion.calendula:       4,
	}
	for key := range powderReagentRoster {
		roster[key] += powderReagentRoster[key] * 1
	}
	return roster
}

type DurabilityOil struct {
	jesterBlood JesterBlood // 1
	monkBranch  string      // 1
	natureFruit string      // 1
	fireDust    string      // 1
}

func NewDurabilityOil() DurabilityOil {
	NewDurabilityOil := DurabilityOil{
		jesterBlood: NewJesterBlood(),
		monkBranch:  monkBranch,
		natureFruit: natureFruit,
		fireDust:    fireDust,
	}
	return NewDurabilityOil
}

func (durabilityOil *DurabilityOil) CountSimpleReagents() map[string]int {
	jesterBloodRoster := durabilityOil.jesterBlood.CountSimpleReagents()
	roster := map[string]int{
		durabilityOil.monkBranch:  1,
		durabilityOil.natureFruit: 1,
		durabilityOil.fireDust:    1,
	}
	for key := range jesterBloodRoster {
		roster[key] += jesterBloodRoster[key] * 1
	}
	return roster
}

type LiquidReagent struct {
	cleanWater string // 1
	weed       string // 1
	calendula  string // 1
	salt       string // 1
}

func NewLiquidReagent() LiquidReagent {
	NewLiquidReagent := LiquidReagent{
		cleanWater: cleanWater,
		weed:       weed,
		calendula:  calendula,
		salt:       salt,
	}
	return NewLiquidReagent
}

func (liquidReagent *LiquidReagent) CountSimpleReagents() map[string]int {
	roster := map[string]int{
		liquidReagent.cleanWater: 1,
		liquidReagent.weed:       1,
		liquidReagent.calendula:  1,
		liquidReagent.salt:       1,
	}
	return roster
}

type PowderReagent struct {
	cleanWater string // 1
	weed       string // 1
	azalea     string // 1
	sugar      string // 1
}

func NewPowderReagent() PowderReagent {
	NewPowderReagent := PowderReagent{
		cleanWater: cleanWater,
		weed:       weed,
		azalea:     azalea,
		sugar:      sugar,
	}
	return NewPowderReagent
}

func (powderReagent *PowderReagent) CountSimpleReagents() map[string]int {
	roster := map[string]int{
		powderReagent.cleanWater: 1,
		powderReagent.weed:       1,
		powderReagent.azalea:     1,
		powderReagent.sugar:      1,
	}
	return roster
}

func PrettyPrint(roster map[string]int) {
	fmt.Println("Полный список ингридиентов для одной попытки крафта зелья Зверя")
	for key, value := range roster {
		fmt.Printf("%s в количестве %d", key, value)
		fmt.Println()
	}
}
