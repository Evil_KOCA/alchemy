package warehouse

type Crafter struct {
	name           string
	equipmentLevel int
	masteryGrade   int
	temporaryBuf   int
}

func NewCrafter(name string, equipmentLevel int, masteryGrade int, temporaryBuf int) Crafter {
	NewCrafter := Crafter{
		name:           name,
		equipmentLevel: equipmentLevel,
		masteryGrade:   masteryGrade,
		temporaryBuf:   temporaryBuf,
	}
	return NewCrafter
}

func (c *Crafter) GetEfficiency(masteryGrade int) (float64, float64) {
	return 2.24, 0.15
}

func (c *Crafter) MakePotions(i TryInterface, attempts int) (common, strong int) {
	commonEfficiency, strongEfficiency := c.GetEfficiency(c.masteryGrade)
	commonRequirements, strongRequirements := i.GetRequirements()
	switch {

	case c.masteryGrade >= strongRequirements && c.masteryGrade >= commonRequirements:
		common = int(commonEfficiency * float64(attempts))
		strong = int(strongEfficiency * float64(attempts))

	case c.masteryGrade < strongRequirements && c.masteryGrade >= commonRequirements:
		common = int(commonEfficiency * float64(attempts))
		strong = 0

	default:
		common, strong = 0, 0
	}

	return common, strong
}
